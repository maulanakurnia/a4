# Whimsical
[Whimsical Wireframes Tutorial](https://youtu.be/Oi9nST5Skog) <br>
[Tutorial Membuat Wireframe Menggunakan Whimsical](https://youtu.be/3Oiy40Tr34w)

## Screenshot
### 1. Web CV
this is example design from YouTube Channel [Andika Satya](https://youtu.be/3Oiy40Tr34w)
![web cv](img/from-andika-satya-channel-yt/web-cv%402x.png)

### 2. Simple Mobile Design
this is example design from YouTube Channel [Whimsical](https://youtu.be/Oi9nST5Skog)
![simple mobile design](img/from-whimsical-channel-yt/simple-mobile-design%402x.png)

### 3. Sport Mobile Design
this is example design from YouTube Channel [Whimsical](https://youtu.be/Oi9nST5Skog)
![Sport Mobile Design](img/from-whimsical-channel-yt/sport-mobile-design%402x.png)

### 4. Cloud Storage Tablet Design
this is example design from YouTube Channel [Whimsical](https://youtu.be/Oi9nST5Skog)
![Cloud Storage](img/from-whimsical-channel-yt/tablet-design%402x.png)

### 5. E-Commerce Web Design
this is example design from YouTube Channel [Whimsical](https://youtu.be/Oi9nST5Skog)
![E-Commerce Web Design](img/from-whimsical-channel-yt/e_commerce-web-design%402x%20(3).png)

### 6. Workflow Auth Design
this is example design from YouTube Channel [Whimsical](https://youtu.be/Oi9nST5Skog)
![Workflow Auth Design](img/from-whimsical-channel-yt/workflow-web-auth%402x%20(4).png)