# Assigment 4 - Intern Privy

Author : Maulana Kurnia Fiqih Ainul Yaqin
<br >
Position : Intern Backend

This repository contains A4 assignment that include:

1. [Whimsical](https://gitlab.com/maulanakurnia/a3/-/tree/main/01-Whimsical)
2. [Trello](https://gitlab.com/maulanakurnia/a4/-/tree/main/02-Trello)

Link Workspace Whimsical:
1. [From Video Andika Satya](https://whimsical.com/web-cv-Hg5FtPwC6eF3vXTHCWGwGo)
2. [From Video Whimsical](https://whimsical.com/wirefrimes-Pf4MXtwtFQyh1J6U32zWNq)

for more example design please open folder [01-Whimsical](https://gitlab.com/maulanakurnia/a4/-/tree/main/01-Whimsical)